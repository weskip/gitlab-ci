#!/bin/bash

aws_registry=237053933029.dkr.ecr.eu-west-3.amazonaws.com
image=dockerci
tag=var

aws ecr get-login-password --region "eu-west-3" | sudo docker login --username AWS --password-stdin $aws_registry

# aws ecr create-repository --repository-name $image

sudo docker image prune --all --filter "until=120h" --force

sudo docker build -t $aws_registry/$image:${tag}1 ~/docker/
sudo docker build -t $aws_registry/$image:${tag}2 ~/docker/
# sudo docker tag $image $aws_registry/$image:$tag
sudo docker push $aws_registry/$image:${tag}1
sudo docker push $aws_registry/$image:${tag}2
